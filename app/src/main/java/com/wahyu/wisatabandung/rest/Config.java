package com.wahyu.wisatabandung.rest;

/**
 * Created by Ujang Wahyu on 17,November,2018
 */
public class Config {
    public static final String BASE_URL_API = "http://www.json-generator.com/api/json/get/";

    public static ApiInterface getAPIService(){
        return ApiClient.getService(BASE_URL_API ).create(ApiInterface.class);
    }
}
