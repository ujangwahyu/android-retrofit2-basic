package com.wahyu.wisatabandung.rest;

import com.wahyu.wisatabandung.model.response.ResponseWisata;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Ujang Wahyu on 17,November,2018
 */
public interface ApiInterface {
    @GET("cgALVqSUJe")
    Call<ResponseWisata> wisata();
}
