package com.wahyu.wisatabandung.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.wahyu.wisatabandung.model.Wisata;

import java.util.List;

/**
 * Created by Ujang Wahyu on 17,November,2018
 */
public class ResponseWisata {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Wisata> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Wisata> getData() {
        return data;
    }

    public void setData(List<Wisata> data) {
        this.data = data;
    }
}
