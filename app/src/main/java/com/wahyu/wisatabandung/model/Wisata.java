package com.wahyu.wisatabandung.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ujang Wahyu on 17,November,2018
 */
public class Wisata {
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("nama_wisata")
    @Expose
    private String namaWisata;
    @SerializedName("gambar")
    @Expose
    private String gambar;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
