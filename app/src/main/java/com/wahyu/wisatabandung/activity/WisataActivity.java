package com.wahyu.wisatabandung.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.wahyu.wisatabandung.R;
import com.wahyu.wisatabandung.adapter.WisataAdapter;
import com.wahyu.wisatabandung.model.Wisata;
import com.wahyu.wisatabandung.model.response.ResponseWisata;
import com.wahyu.wisatabandung.rest.ApiInterface;
import com.wahyu.wisatabandung.rest.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WisataActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    WisataAdapter wisataAdapter;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wisata);

        findView();
        initView();
        initListener();
    }

    public void findView(){
        recyclerView = findViewById(R.id.recycler_view);
    }

    public void initView(){
        apiInterface = Config.getAPIService();
        loadDataWisata();
    }

    public void initListener(){
        Call<ResponseWisata> call = apiInterface.wisata();

        call.enqueue(new Callback<ResponseWisata>() {
            @Override
            public void onResponse(Call<ResponseWisata> call, Response<ResponseWisata> response) {
                if (response.isSuccessful()){
                    listWisata(response.body().getData());

                }else {
                    Toast.makeText(WisataActivity.this, "Gagal Mengambil Data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWisata> call, Throwable t) {
                Toast.makeText(WisataActivity.this, "Koneksi Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void listWisata(List<Wisata> wisataList){
        wisataAdapter = new WisataAdapter(this, wisataList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(wisataAdapter);

    }

    public void loadDataWisata(){

    }
}
