package com.wahyu.wisatabandung.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wahyu.wisatabandung.R;
import com.wahyu.wisatabandung.model.Wisata;
import java.util.List;

/**
 * Created by Ujang Wahyu on 17,November,2018
 */

public class WisataAdapter extends RecyclerView.Adapter<WisataAdapter.MyViewHolder> {
    private Context mContext;
    private List<Wisata> wisataList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgCover;
        private TextView txtNama, txtDeskripsi;

        public MyViewHolder(View view) {
            super(view);
            imgCover = view.findViewById(R.id.img_gambar);
            txtNama = view.findViewById(R.id.tv_nama);
            txtDeskripsi = view.findViewById(R.id.tv_deskripsi);
        }
    }

    public WisataAdapter(Context mContext, List<Wisata> wisatas) {
        this.mContext = mContext;
        this.wisataList = wisatas;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_wisata, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Wisata wisata = wisataList.get(position);
        holder.txtNama.setText(wisata.getNamaWisata());
        holder.txtDeskripsi.setText(wisata.getDeskripsi());

        Glide.with(mContext)
                .load(wisata.getGambar())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(holder.imgCover);
    }

    @Override
    public int getItemCount() {
        return wisataList.size();
    }
}
